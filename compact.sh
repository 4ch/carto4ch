#!/bin/bash

# Add /usr/local/bin directory where docker-compose is installed
#PATH=/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin

cd /home/semapps/carto4ch

# Stop all containers including Fuseki
/usr/bin/docker compose -f docker-compose-prod.yaml stop fuseki

# Run fuseki compact with same data as prod
/usr/bin/docker run --volume="$(pwd)"/data/fuseki:/fuseki --entrypoint=/docker-compact-entrypoint.sh semapps/jena-fuseki-webacl

/usr/bin/docker compose -f docker-compose-prod.yaml up -d fuseki

echo "Cron job finished at" $(date)
