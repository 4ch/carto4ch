import config from "./config";

const dataServers = {
  a: {
    name: 'Server A',
    baseUrl: config.middlewareUrl,
    authServer: true,
    default: true,
    uploadsContainer: '/files'
  }
  // b: {
  //   name: 'Server B',
  //   baseUrl: "https://data.carto4ch.huma-num.fr/",
  // }
};

export default dataServers;
