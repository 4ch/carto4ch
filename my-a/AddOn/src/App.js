import React from 'react';
import { Admin, Resource, memoryStore, Notification } from 'react-admin';
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import LoginPage from './layout/LoginPage';
import { BrowserRouter } from 'react-router-dom';

import i18nProvider from './config/i18nProvider';
import authProvider from './config/authProvider';
import dataProvider from './config/dataProvider';
import theme from './config/theme';
import HomePage from "./HomePage";
import * as resources from './resources';

import MyLayout from './layout/MyLayout';

theme.palette.primary.main = "#AE4796"

const App = () => (
  <StyledEngineProvider injectFirst>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <Admin
          disableTelemetry
          title="My Server A"
          authProvider={authProvider}
          dataProvider={dataProvider}
          i18nProvider={i18nProvider}
          theme={theme}
          layout={MyLayout}
          loginPage={LoginPage}
          store={memoryStore()}
          dashboard={HomePage}
          notification={Notification}
        >
          {Object.entries(resources).map(([key, resource]) => (
            <Resource key={key} name={key} {...resource.config} />
          ))}
        </Admin>
      </ThemeProvider> 
    </BrowserRouter>
  </StyledEngineProvider>
);

export default App;
