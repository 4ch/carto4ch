import React from 'react';
import { Notification } from 'react-admin';
import { Container, Zoom, Box, useMediaQuery, ThemeProvider, makeStyles, Typography } from '@material-ui/core';
import ScrollToTop from './ScrollToTop';
import { UserMenu, LogoutButton } from '@semapps/auth-provider';
import { Link } from 'react-admin';

const useStyles = makeStyles(theme => ({
  hero: {
    backgroundColor: "#AE4796",
  },
  userMenu: {
    float: 'right',
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor: "#AE4796",
    '& button': {
      padding: '6px 12px'
    }
  },
  presContainer: {
    flex: 1,
    overflow: 'hidden',
    [theme.breakpoints.up('sm')]: {
      flex: 'unset',
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center'
    }
  },
  logoContainer: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      height: 48,
      marginLeft: '0.2em',
      marginRight: '0.2em',
      display: 'block'
    }
  },
  logo: {
    height: '100%'
  },
  title: {
    display: 'block',
    color: theme.palette.common.white,
    [theme.breakpoints.up('sm')]: {
      display: 'none'
    },
    [theme.breakpoints.up('md')]: {
      display: 'block'
    }
  },
  footer: {
    '@media print': {
      display: 'none'
    }
  },
  footerLink: {
    color: theme.palette.grey["500"],
    '&:hover': {
      textDecoration: 'underline'
    }
  }
}));

const Layout = ({ title, open, logout, theme, children }) => {
  const classes = useStyles();
  const xs = useMediaQuery(theme.breakpoints.down('xs'));
  console.log("theme color=",theme.palette.primary.main);
  return (
    <ThemeProvider theme={theme}>
      <ScrollToTop />
      <Box width={1} height="60px" className={classes.hero}>
        <Container>
          <UserMenu logout={<LogoutButton />} classes={{ user: classes.userMenu }} />
          <Link to="/">
            <div className={classes.presContainer}>
              <div className={classes.logoContainer}>
                <Zoom in={true} timeout={2000}>
                  <img className={classes.logo} src={process.env.PUBLIC_URL + '/logo192.png'} alt="logo" />
                </Zoom>
              </div>
              <Typography variant="h4"  className={classes.title} component="h4" align="center">
                {title}
              </Typography>
            </div>
          </Link>
        </Container>
      </Box>
      <Container maxWidth="md" disableGutters={xs}>
        <Box m={{ xs: 1, sm: 3 }}>{children}</Box>
        <Box mb={{ xs: 0, sm: 3 }} className={classes.footer}>
          <Typography variant="subtitle2" color="textSecondary" align="right">
            <a href="https://semapps.org/" target="_blank" rel="noopener noreferrer" className={classes.footerLink}>Plateforme propulsée par SemApps</a>
            &nbsp;|&nbsp;
            <a href="https://portal.carto4ch.huma-num.fr/" target="_blank" rel="noopener noreferrer" className={classes.footerLink}>Pour le projet Carto4CH</a>
          </Typography>
        </Box>
      </Container>
      {/* Required for react-admin optimistic update */}
      <Notification />
    </ThemeProvider>
  );
};

export default Layout;
