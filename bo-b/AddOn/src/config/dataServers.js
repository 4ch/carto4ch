import config from "./config";
const dataServers = {
  b: {
    name: 'Server B',
    baseUrl: config.middlewareUrl,
    authServer: true,
    default: true,
    uploadsContainer: '/files'
  },
  a: {
    name: 'Server A',
    baseUrl: "https://data-a.carto4ch.huma-num.fr/",
  }
};

export default dataServers;
