const { MirrorService } = require('@semapps/sync');
const CONFIG = require('../config/config');

module.exports = {
  mixins: [MirrorService],
  settings: {
    graphName: 'http://semapps.org/mirror',
    servers: ['https://data-a.carto4ch.huma-num.fr', 'https://data-b.carto4ch.huma-num.fr']
  }
};